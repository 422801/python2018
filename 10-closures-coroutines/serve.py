# script-URI = <scheme> "://" <server-name> ":" <server-port>
                #    <script-path> <extra-path> "?" <query-string>

# internal server error inak 200
# ak neobsahuje hlavicky, musia tam byt hlavicky a musi tam byt content type

import sys
import os
import http.server
import socketserver
from http.server import CGIHTTPRequestHandler
import urllib

class Server(socketserver.ThreadingMixIn, http.server.HTTPServer):
    pass

def handler():
    class cgi_handler(CGIHTTPRequestHandler):
        # given_dir = None
        
        def do_GET(self):
            self.do_ALL()

        def do_POST(self):
            self.do_ALL()

        def do_HEAD(self):
            self.do_ALL()

        def do_ALL(self):
            # given_path = os.path.normpath(self.given_dir + self.path)
            # print(given_path)
            # print("--------------------")
            # print(self.path[1:])
            # print(self.is_cgi())
            actual_dir = os.path.dirname(self.path)
            if (actual_dir not in self.cgi_directories):
                self.cgi_directories.append(actual_dir)

            
            file_name_url = urllib.parse.urlparse(self.path).path[1:]
            # print(file_name_url)
            if (self.is_cgi() and file_name_url.endswith('.cgi')):
                self.run_cgi()
            else:
                # static data with simplehttprequesthandler  
                file_out = http.server.SimpleHTTPRequestHandler.send_head(self)
                if (file_out):
                    try:
                        self.copyfile(file_out, self.wfile)
                    finally:
                        file_out.close()


            # if (os.path.isfile(given_path)):
            #     if (given_path.endswith('.cgi')):
            #         p = os.path.relpath(given_path)
            #         #self.cgi_info = os.path.dirname(p), os.path.basename(p) 
            #         self.run_cgi()
            #     else:
            #         with open(given_path, 'rb') as f:
            #             self.send_response(200) 
            #             self.send_header("Content-Length", os.path.getsize(given_path))
            #             self.end_headers()
            #             content = f.read(-1)
            #             while content:
            #                 self.wfile.write(content) 
            #                 content = f.read(-1)
            # else:
            #     self.send_error(404, explain="File %s not found." % given_path)            
    return cgi_handler

def main():
    if (len(sys.argv) is not 3):
        print("Arguments should be like: script_name port directory")
    else:
        port = int(sys.argv[1])
        directory = sys.argv[2]

        os.chdir(os.path.realpath(directory))
        cgi_handler = handler()
        httpd = Server(('localhost', port), cgi_handler)
        try: 
            httpd.serve_forever()
        except KeyboardInterrupt: 
            pass
            httpd.server_close()

if __name__ == '__main__':
    main()