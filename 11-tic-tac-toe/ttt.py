'''
otazky - 

prrobit do hry sa nepripajas ale hned hras



• write a game server for (3x3) tic tac toe
• invocation: ./ttt.py port
− listen on the given port (number)
− serve HTTP (only GET requests)
− all responses are JSON dictionaries

START
GET /start?name=string
• returns a numeric id
− multiple games may run in parallel
• the game starts with a

STATUS
• GET /status?game=id
• if the game is over:
− set winner to 0 (draw), 1 or 2
• otherwise set:
− self.board is a list of lists of numbers
− 0 = empty, 1 and 2 indicate the player
− next 1 or 2 (who plays next)

PLAYING
GET /play?game=id&player=1&x=1&y=2
• must validate the request
• set status to either "ok" or "bad"
− if status is "bad", set message
− message is free-form text for the user



curl --get localhost:9001/status?game=id
curl --get localhost:9001/play?game=1^&player=1^&x=0^&y=2


new player is connecting to server, can create new game or select one from list

when creating new game, is like player 1, when joining is player 2
player 1 starts game

'''
import sys
from http.server import HTTPServer, BaseHTTPRequestHandler
import urllib
from aiohttp import web
import json
import asyncio


def get_game(input_id):
    for game in GameServer.all_games:
        if (game.id_of_game == int(input_id)):
            return game
    return None


async def process_start_request(request):
    input_query = request.query_string
    if (input_query[0:5] == 'name=' or input_query == ''):
        name = input_query[5:]
        new_game = Game(name)
        GameServer.all_games.append(new_game)
        output = {}
        output['id'] = new_game.game_id_count
        return web.Response(text=json.dumps(output, indent=4, ensure_ascii = False))
    else:
        return web.Response(text="Wrong start request.", status = 404)


async def process_status_request(request):
    input_query = request.query_string
    output = {}
    if (input_query[0:5] == 'game='):
        game_id = input_query[5:]

        if (len(game_id) is 0):
            return web.Response(text="Wrong status request, no ID input.", status = 404)
            
        else:
            searched_game = get_game(game_id)
            if (searched_game is None):
                return web.Response(text="Game with given ID does not exist.", status = 404)
            else:
                output = searched_game.status()
                return web.Response(text=json.dumps(output, indent=4, ensure_ascii = False))
    else:
        return web.Response(text="Wrong status request.", status = 404)


async def process_play_request(request):
    params = request.query
    output = {}

    if ('game' in params and 'player' and params and 'x' in params and 'y' in params):

        game_id = params['game']
        player_numb = params['player']
        row = params['x']
        column = params['y']

        search_game = get_game(game_id)
        if (search_game is None):    
            return web.Response(text="Wrong game ID.", status = 404)

        elif(search_game.is_finished):               
            output['status'] = 'bad'
            output['message'] = "Game is over."
            return web.Response(text=json.dumps(output, indent=4, ensure_ascii = False), status = 200)
        else:
            if ((int(player_numb) != 1 and int(player_numb) != 2) or int(player_numb) != search_game.whos_turn):
                output['status'] = 'bad'
                output['message'] = "Wrong player number."
                return web.Response(text=json.dumps(output, indent=4, ensure_ascii = False), status = 200)

            try:
                if (int(row) not in range(0, 3) or int(column) not in range(0, 3)):
                    output['status'] = 'bad'
                    output['message'] = "Wrong move in game, x and y must be 0, 1 or 2."
                    return web.Response(text=json.dumps(output, indent=4, ensure_ascii = False), status = 200)
            except:
                output['status'] = 'bad'
                output['message'] = "x and y must be number."
                return web.Response(text=json.dumps(output, indent=4, ensure_ascii = False), status = 200)
            
            if (search_game.board[int(row)][int(column)] == 0):
                search_game.board[int(row)][int(column)] = search_game.whos_turn
        
                if (search_game.whos_turn == 1):
                    search_game.whos_turn = 2 
                else:
                    search_game.whos_turn = 1     

                output['status'] = 'ok'

                is_game_done = {}
                is_game_done = search_game.status()
                if ('winner' in is_game_done):
                    search_game.is_finished = True

                return web.Response(text=json.dumps(output, indent=4, ensure_ascii = False), status = 200)
            else:
                output['status'] = 'bad'
                output['message'] = "Given position is already marked"
                return web.Response(text=json.dumps(output, indent=4, ensure_ascii = False), status = 200)
    else:
        return web.Response(text="Wrong play request.", status = 404)

             
class GameServer:
    all_games = []

class Game:
    game_id_count = 0
    name = ''
    board = []
    whos_turn = None
    id_of_game = None
    is_finished = False

    def __init__(self, name):
        Game.game_id_count = Game.game_id_count + 1
        self.id_of_game = Game.game_id_count
        self.board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        self.name = name
        self.whos_turn = 1
        self.is_finished = False

    def status(self):
        output = {}

        for player_num in range (1, 3):
            # rows
            if (len(set([self.board[0][0], self.board[0][1], self.board[0][2], player_num])) == 1):
                output['winner'] = player_num
            elif (len(set([self.board[1][0], self.board[1][1], self.board[1][2], player_num])) == 1):
                output['winner'] = player_num
            elif (len(set([self.board[2][0], self.board[2][1], self.board[2][2], player_num])) == 1):
                output['winner'] = player_num
            #columns
            elif (len(set([self.board[0][0], self.board[1][0], self.board[2][0], player_num])) == 1):
                output['winner'] = player_num
            elif (len(set([self.board[0][1], self.board[1][1], self.board[2][1], player_num])) == 1):
                output['winner'] = player_num
            elif (len(set([self.board[0][2], self.board[1][2], self.board[2][2], player_num])) == 1):
                output['winner'] = player_num
            # diagonal
            elif (len(set([self.board[0][0], self.board[1][1], self.board[2][2], player_num])) == 1):
                output['winner'] = player_num
            elif (len(set([self.board[0][2], self.board[1][1], self.board[2][0], player_num])) == 1):
                output['winner'] = player_num


        if (len(output) == 0):
            if (any(0 in num for num in self.board)):
                output['board'] = self.board
                output['next'] = self.whos_turn 
            else:
                output['winner'] = 'draw'

        return output

def main():
    if (len(sys.argv) >= 2):
        port = sys.argv[1]
    else:
        port = input("Enter the port:") 


    # if sys.platform.startswith('win'):
    #     loop = asyncio.ProactorEventLoop()
    #     asyncio.set_event_loop(loop)
    my_server = web.Application()

    my_server.add_routes([web.get('/start{tail:.*}', process_start_request),
                web.get('/status{tail:.*}', process_status_request),
                web.get('/play{tail:.*}', process_play_request)])

    web.run_app(my_server, host='localhost', port=port)

if __name__ == "__main__":
    main()
