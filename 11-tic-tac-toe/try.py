'''
• write a game server for (3x3) tic tac toe
• invocation: ./ttt.py port
− listen on the given port (number)
− serve HTTP (only GET requests)
− all responses are JSON dictionaries

START
GET /start?name=string
• returns a numeric id
− multiple games may run in parallel
• the game starts with a

STATUS
• GET /status?game=id
• if the game is over:
− set winner to 0 (draw), 1 or 2
• otherwise set:
− self.board is a list of lists of numbers
− 0 = empty, 1 and 2 indicate the player
− next 1 or 2 (who plays next)

PLAYING
GET /play?game=id&player=1&x=1&y=2
• must validate the request
• set status to either "ok" or "bad"
− if status is "bad", set message
− message is free-form text for the user



curl -G localhost:9001/status?game=id



new player is connecting to server, can create new game or select one from list

when creating new game, is like player 1, when joining is player 2
player 1 starts game

'''
import sys
from http.server import HTTPServer, BaseHTTPRequestHandler
import urllib
from aiohttp import web
import json

class requestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        file_name_url = urllib.parse.urlparse(self.path)
        input_path = file_name_url[2][1:]
        input_query = file_name_url[4]
        if (input_path == 'start'):
            self.process_start_request(input_query)
        elif (input_path == 'status'):
            self.process_status_request(input_query)
        elif (input_path == 'play'):
            self.process_play_request(input_query)

    def process_start_request(self, input_query):
        if (input_query[0:5] == 'name=' or input_query == ''):
            name = input_query[5:]
            if (name not in GameServer.all_game_names):
                new_game = Game(name)
                GameServer.all_game_names.append(name)
                GameServer.all_games.append(new_game)
                first_player = Player(1)
                new_game.players.append(first_player)
                new_game.whos_turn = first_player.number
                output = {}
                output['id'] = new_game.game_id_count
                self.send_output(output)
            else:
                for game in GameServer.all_games:
                    if (game.name == name):
                        current_game = game
                if (len(current_game.players) == 1):
                    second_player = Player(2)
                    current_game.players.append(second_player)
                    output = {}
                    output['id'] = current_game.id_of_game
                    self.send_output(output)
                else:
                    self.send_error(404, explain="Game is full.")
        else:
            self.send_error(404, explain="Wrong start request.")

    def process_status_request(self, input_query):
        output = {}
        if (input_query[0:5] == 'game='):
            game_id = input_query[5:]

            if (len(game_id) is 0):
                self.send_error(404, explain="Wrong status request, no ID input.")
            else:
            # searched_game = None
            # for game in GameServer.all_games:
            #     if (game.game_id_count == int(game_id)):
                searched_game = self.get_game(game_id)
                if (searched_game is None):
                    self.send_error(404, explain="Game with given ID does not exist.")
                else:
                    output = searched_game.status()
                    self.send_output(output)
        else:
            self.send_error(404, explain="Wrong status request.")
                        
    def process_play_request(self, input_query):
        output = {}
        if (input_query[0:5] == 'game='):
            all_query = input_query.split('&')
            if (len(all_query) == 4):
                game_id = all_query[0][5:]
                player_numb = all_query[1][7:]
                row = all_query[2][2:]
                column = all_query[3][2:]

                
                search_game = self.get_game(game_id)
                if (search_game is None):    
                    self.send_error(404, explain="Wrong game ID.")
                else:
                    if ((int(player_numb) != 1 and int(player_numb) != 2) or int(player_numb) != search_game.whos_turn):
                        # self.send_error(404, explain="Wrong player number.")
                        output['status'] = 'bad'
                        output['message'] = "Wrong player number."
                        self.send_output(output)

                    try:
                        if (int(row) not in range(0, 3) or int(column) not in range(0, 3)):
                            output['status'] = 'bad'
                            output['message'] = "Wrong move in game, x and y must be 0, 1 or 2."
                            self.send_output(output)
                    except:
                        output['status'] = 'bad'
                        output['message'] = "x and y must be number."
                        self.send_output(output)
                    
                    if (len(search_game.players) == 1):
                        output['status'] = 'bad'
                        output['message'] = "Cannot play without second player."
                        self.send_output(output)
                    else:
                        if (search_game.board[int(row)][int(column)] == 0):
                            search_game.board[int(row)][int(column)] = search_game.whos_turn
                                            
                            if (search_game.whos_turn == 1):
                                search_game.whos_turn = 2 
                            else:
                                search_game.whos_turn = 1     

                            output['status'] = 'ok'
                            self.send_output(output)
                        else:
                            output['status'] = 'bad'
                            output['message'] = "Given position is already marked"
                            self.send_output(output)

            else:
                self.send_error(404, explain="Wrong play request.")
        else:
            self.send_error(404, explain="Wrong play request.")

                

    def get_game(self, input_id):
        for game in GameServer.all_games:
            if (game.id_of_game == int(input_id)):
                return game
        return None

    def send_output(self, output):
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        self.wfile.write(bytes(json.dumps(output, indent=4, ensure_ascii = False), 'UTF-8'))
    

class GameServer:
    all_game_names = []
    all_games = []
        
    # def get_game(self, game_id):
    #     for game in self.all_games:
    #         if (game.game_id_count == int(game_id)):
    #             return game

    def __init__(self, port, handler):
        httpd = HTTPServer(("localhost", int(port)), handler)
        try: 
            httpd.serve_forever()
        except KeyboardInterrupt: 
            pass
            httpd.server_close()

class Game:
    game_id_count = 0
    players = []
    name = ''
    board = []
    whos_turn = None
    id_of_game = None

    def __init__(self, name):
        Game.game_id_count = Game.game_id_count + 1
        self.id_of_game = Game.game_id_count
        self.board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        self.name = name
        self.players = []
        self.whos_turn = None

    def status(self):
        output = {}

        for player_num in range (1, 3):
            # rows
            if (len(set([self.board[0][0], self.board[0][1], self.board[0][2], player_num])) == 1):
                output['winner'] = player_num
            elif (len(set([self.board[1][0], self.board[1][1], self.board[1][2], player_num])) == 1):
                output['winner'] = player_num
            elif (len(set([self.board[2][0], self.board[2][1], self.board[2][2], player_num])) == 1):
                output['winner'] = player_num
            #columns
            elif (len(set([self.board[0][0], self.board[1][0], self.board[2][0], player_num])) == 1):
                output['winner'] = player_num
            elif (len(set([self.board[0][1], self.board[1][1], self.board[2][1], player_num])) == 1):
                output['winner'] = player_num
            elif (len(set([self.board[0][2], self.board[1][2], self.board[2][2], player_num])) == 1):
                output['winner'] = player_num
            # diagonal
            elif (len(set([self.board[0][0], self.board[1][1], self.board[2][2], player_num])) == 1):
                output['winner'] = player_num
            elif (len(set([self.board[0][2], self.board[1][1], self.board[2][0], player_num])) == 1):
                output['winner'] = player_num


        if (len(output) == 0):
            if (any(0 in num for num in self.board)):
                output['board'] = json.dumps(self.board)
                output['next'] = self.whos_turn 
            else:
                output['winner'] = 'draw'
                self.end_game()
        else:
            self.end_game()

        return output
        
    # delete game
    def end_game(self):
        GameServer.all_game_names.remove(self.name)
        GameServer.all_games.remove(self)
        
class Player:
    count = 0
    number = None   # new player, number 1 if creator of game or 2 if joined game

    def __init__(self, number):
        Player.count = Player.count + 1
        self.id = Player.count
        self.number = number



def main():
    if (len(sys.argv) >= 2):
        port = sys.argv[1]
    else:
        port = input("Enter the port:") 

    # httpd = HTTPServer(("localhost", int(port)), requestHandler)
    # try: 
    #     httpd.serve_forever()
    # except KeyboardInterrupt: 
    #     pass
    #     httpd.server_close()

    GameServer(port, requestHandler)

if __name__ == "__main__":
    main()

