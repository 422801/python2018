import sys 
import aiohttp
import asyncio
import http.client
import json
import re


class Game(object):   
    def __init__(self, id_of_game):
        self.winner = None
        self.turn = 1
        self.id = id_of_game

class Player(object):
    def __init__(self, player_number):
        self.num = player_number
        self.piece = 'x' if player_number == 1 else 'o'


async def new_game(client, url, name = None):
    if (name is not None):      
        url = url + '/start?name=%s' % name 
    else:
        url = url + '/start?name='

    async with client.get(url) as resp:

        if (resp.status == 200):
            text = json.loads(await resp.text())
            print("New game created.")

            game = Game(int(text['id']))
            player = Player(1)
            return game, player
        else:
            print(await resp.text())
            return None, None
   

async def join_game(client, url, id_of_game):
    req_url = url + "/join?game=%s" % str(id_of_game)
    async with client.get(req_url) as resp:
        if (resp.status == 200):
            game = Game(id_of_game)
            player = Player(2)
            print(await resp.text())
            return game, player
        else:
            print(await resp.text())
            return None, None
            

async def waiting_for_player(client, url, game):
    url = url + "/full?game=%s" % game.id
    async with client.get(url) as resp:
        if (resp.status == 200):
            return True
        else:
            return False


async def game_status(client, url, game, player, just_check = None):
    url = url + "/status?game=%s" % game.id
    async with client.get(url) as resp:
        if (resp.status == 200):
            text = json.loads(await resp.text())
            if ('winner' in text):
                if (text['winner'] == 'draw'):
                    game.winner = 'draw'
                    
                    if (just_check is None):
                        print('draw') 

                else:
                    game.winner = int(text['winner'])
                    if (player.num == game.winner):

                        if (just_check is None):
                            print('you win')

                    else:

                        if (just_check is None):
                            print('you lose')

            elif ('board' in text):
                board = text['board']

                for row in board:
                    line = ''
                    for data in row:
                        if (data == 0):
                            data = '_'
                        if (data == 1):
                            data = 'x'
                        if (data == 2):
                            data = 'o'
                        line += data
                    if (just_check is None):    
                        print(line)

                is_next = text['next']
                return is_next
      
            else:
                if (just_check is None):
                    print(text)            
        else:
            if (just_check is None):
                print(await resp.text())
    return None


async def play_game(client, url, game, player, x, y):
    url = url + ("/play?game=%s&player=%s&x=%s&y=%s" % (game.id, player.num, x, y))
    async with client.get(url) as resp:
        if (resp.status == 200):
            text = json.loads(await resp.text())
            if (text['status'] == 'ok'):
                if (game.turn == 1):
                    game.turn = 2
                else:
                    game.turn = 1
            else:
                print(text)            
        else:
            print(await resp.text())


def check_game_input(player):
    try:
        x, y = input("your turn (%s): " % player.piece).split()   # input divided by space - split()
        while (int(x.strip()) not in range(0, 3) or int(y.strip()) not in range(0, 3)):
            print("invalid input")
            x, y = input("your turn (%s): " % player.piece).split()
        return x, y
    except:
        print("invalid input")
        return check_game_input(player)


async def client_sesn(url):
    async with aiohttp.ClientSession() as client:

        game = None
        async with client.get(url + "/list") as resp:
            print("Games avaible:")
            games = json.loads(await resp.text())

            for game in games:
                print(str(game['id']) + " " + game['name'])

            # print(json.dumps(games, indent=4))

        count = len(games)
        if (count == 0):
            m = None
            while (m is None):
                text = input("No games avaible. Write 'new' to start new game: ")
                r = re.compile('new(.*)')
                m = r.match(text)
                if (m is not None):
                    game, player = await new_game(client, url, m.group(1).strip())              
            
        else:    
            id_of_game = input("Select id of game you want to play or type 'new' to start new game: ")
            r = re.compile('new(.*)')
            m = r.match(id_of_game)
            
            if (m is not None):        
                game, player = await new_game(client, url, m.group(1).strip())
            else:
                try:
                    id_of_game = int(id_of_game)
                    game, player = await join_game(client, url, id_of_game)
                except:
                    game, player = await join_game(client, url, id_of_game)
                    await client_sesn(url)

        # game
        if (game is not None):
            
            if (player.num == 1):
                print("waiting for the other player to join")
            else:
                print("waiting for the other player")
                
            while True:
                if (await waiting_for_player(client, url, game) == False):
                    status = await asyncio.wait_for(game_status(client, url, game, player, True), timeout=1.0)

                    if (status is not None):
                        
                        if (int(status) == player.num):
                            await game_status(client, url, game, player)
                            x, y = check_game_input(player)
                            await play_game(client, url, game, player, x, y)                 
                            await game_status(client, url, game, player, True) # if player do last turn
                            if (game.winner is None):
                                await game_status(client, url, game, player)
                                print("waiting for the other player")
                            
                    if (game.winner is not None):
                        await game_status(client, url, game, player)
                        break
                    
            again = input("Would you like to play again? 'y' or 'n': ")
            if (again == 'y'):
                await client_sesn(url)
            else:
                exit()

async def main():
    if (len(sys.argv) >= 3):
        host = sys.argv[1]
        port = sys.argv[2]
    else:
        host, port = input("Enter host and port: ") 

    if (host[:4] != http):
        host = 'http://' + host
    if (host[-1] == '/'):
        host[-1] = '' 
    
    url = host + ':' + port
    await client_sesn(url)
           

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main()) 