import json
import sys
import csv
import pandas as pd
import datetime as dt
import numpy as np

# return all unique exercises and dates
def allUniqueExercisesAndDates(header):
    exercises = set()
    dates = set()
    for data in header:
        line = data.split('/')
        if (len(line) == 2):
            exercises.add(line[1])
            dates.add(line[0])
    return sorted(exercises), sorted(dates)

# return stats from exercises
def selectedStudentExercises(idNum, df_student, exercises):
    total = 0
    passed = 0
    allData = []
    points = 0

    for exercise in exercises:
        
        df_exercise = df_student.loc[:, df_student.columns.str.endswith('/' + exercise)]
        df_final = df_exercise.sum(axis=1)
        points = df_final.get(idNum)

        if (points > 0):
            passed += 1
        
        total += points
        allData.append(points)
    data = {idNum: allData}
    df_output = pd.DataFrame.from_dict(data)

    statistics = {}
    statistics['mean'] = (df_output.mean()).get(idNum)
    statistics['median'] = (df_output.median()).get(idNum)
    statistics['total'] = total
    statistics['passed'] = passed
    return statistics

# return stats from dates
def selectedStudentDates(idNum, df_student, dates):
    SEMESTER_START = dt.datetime.strptime('2018-09-17', '%Y-%m-%d').date().toordinal() # Gregorian ordinal of the date
    statistics = {}

    allPoints = []
    ordinalDates = []

    sum = 0
    for date in dates:
        df_exercise = df_student.loc[:, df_student.columns.str.startswith(date)]
        df_final = df_exercise.sum(axis=1)
        sum += df_final.get(idNum)
        allPoints.append(sum)
        ordinalDates.append(dt.datetime.strptime(date, '%Y-%m-%d').date().toordinal() - SEMESTER_START)
    suma = 0
    dela = 0
    slope = 0
    points16 = 'inf'
    points20 = 'inf'

    if (allPoints[-1] != 0):
        for i in range(len(allPoints)):
            suma = suma + (ordinalDates[i] * allPoints[i])
            dela = dela + (ordinalDates[i] * ordinalDates[i])

        slope = suma / dela
        points16 = dt.datetime.fromordinal(int(16 / slope) + SEMESTER_START).date()
        points20 = dt.datetime.fromordinal(int(20 / slope) + SEMESTER_START).date()
        
    statistics["regression slope"] = slope
    statistics["date 16"] = str(points16)
    statistics["date 20"] = str(points20)
    return statistics

# chcek ID and return dataframe to process
def checkIdInput(idInput, df):
    if (idInput == 'average'):
        idNum = idInput
        df.set_index('student', inplace=True)
        df.loc['average'] = df.mean()
        df_student = df[df.index == idInput]
    elif (idInput.isdigit()):        
        idNum = int(idInput)
        df_student = df[df.student == int(idInput)]
        if (df_student.empty):
            raise ValueError("Id not in DataFrame")
        df_student.set_index('student', inplace=True)
    else:
        raise ValueError("Wrong <id> input")
        
    return idNum, df_student

def main():
    fileInput = sys.argv[1]
    idInput = sys.argv[2] # <id> is the student identifier or average

    df = pd.read_csv(fileInput, delimiter = ',')
    idNum, dataFrame = checkIdInput(idInput, df)

    headers = sorted(df.columns.values.tolist())
    exercises, dates = allUniqueExercisesAndDates(headers)

    stat1 = selectedStudentExercises(idNum, dataFrame, exercises)
    stat2 = selectedStudentDates(idNum, dataFrame, dates)

    finalStat = {**stat1, **stat2}
    print(json.dumps(finalStat, indent=4))

if __name__ == "__main__":
    main()