import json
import sys
import csv
import pandas as pd

# return all unique exercises and dates
def allUniqueExercisesAndDates(header):
    exercises = set()
    dates = set()
    for data in header:
        line = data.split('/')
        exercises.add(line[1])
        dates.add(line[0])
    return sorted(exercises), sorted(dates)

# deadline mode selection from input
def modeDeadlines(df, headers):
    output = {}
    for head in headers:
        line = {}
        mean = df.loc[:,head].mean()
        median = df.loc[:,head].median()
        first = df.loc[:,head].quantile(q=0.25)
        last = df.loc[:,head].quantile(q=0.75)

        passed = df.loc[:,head][df.loc[:,head] > 0].count()

        line = {"mean": mean, "median": median, "first": first, "last": last, "passed": int(passed)}
        output.update({head: line})
    output = json.dumps(output, indent=4, ensure_ascii=False)
    print(output)

# return one line of json file with data from data frame
def outputLine(df_final):
    mean = df_final.mean()
    median = df_final.median()
    first = df_final.quantile(q=0.25)
    last = df_final.quantile(q=0.75)
    passed = df_final[df_final > 0].count()
    return {"mean": mean, "median": median, "first": first, "last": last, "passed": int(passed)}

# exercises mode selection from input
def modeExercises(df, exercises):
    output = {}
    for exercise in exercises:

        df_exercise = df.loc[:, df.columns.str.endswith('/' + exercise)]
        df_final = df_exercise.sum(axis=1)

        line = outputLine(df_final)
        output.update({exercise: line})
    output = json.dumps(output, indent=4, ensure_ascii=False)
    print(output)

# dates mode selection from input
def modeDates(df, dates):
    output = {}
    for date in dates:

        df_date = df.loc[:, df.columns.str.startswith(date)]
        df_final = df_date.sum(axis=1)
        
        line = outputLine(df_final)
        output.update({date: line})
    output = json.dumps(output, indent=4, ensure_ascii=False)
    print(output)

# write stats from csv file by given mode
def main():
    fileInput = sys.argv[1]
    mode = sys.argv[2]

    df = pd.read_csv(fileInput, delimiter = ',')
    df.set_index('student', inplace=True)
    headers = sorted(df.columns.values.tolist())
    exercises, dates = allUniqueExercisesAndDates(headers)

    if (mode == 'dates'):
        modeDates(df, dates)
    elif (mode == 'exercises'):
        modeExercises(df, exercises)
    elif (mode == 'deadlines'):
        modeDeadlines(df, headers)

if __name__ == "__main__":
    main()