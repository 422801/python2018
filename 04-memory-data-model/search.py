# input is composer name substring
# output is a list of all mtching composer names along with all their prints in the database

# ... where person.name like "%Bach%"

import sqlite3
import sys
import json

composerName = sys.argv[1]

datFile = "scorelib.dat"
conn = sqlite3.connect(datFile)
c = conn.cursor()

output = {}
searchingName = None

c.execute('SELECT id, name FROM person WHERE name LIKE (?)', ['%'+composerName+'%'])
ids = c.fetchall()

for id in ids:
    allComposers = []
    personId = id[0]
    searchingName = id[1]

    c.execute('SELECT score FROM score_author WHERE composer = (?)', [personId])
    scoreIds = c.fetchall()
    if (len(scoreIds) > 0):

        for scoId in scoreIds:
            c.execute('SELECT * FROM score WHERE id = (?)', [scoId[0]])
            scoreInfo = c.fetchall()
            title = scoreInfo[0][1]
            genre = scoreInfo[0][2]
            key = scoreInfo[0][3]
            incipit = scoreInfo[0][4]
            compositionYear = scoreInfo[0][5]

            c.execute('SELECT number, range, name FROM voice WHERE score = (?)', [scoId[0]])
            voiceInfo = c.fetchall()

            voices = {}
            for voice in voiceInfo:
                voiceLine = {str(voice[0]): { "Name": voice[2], "Range": voice[1] }}
                voices.update(voiceLine)

            c.execute('SELECT name, born, died FROM person JOIN score_author ON person.id = score_author.composer WHERE score_author.score = (?)', [scoId[0]])
            authorInfo = c.fetchall()

            composers = []
            for author in authorInfo:
                nameAu = author[0]
                bornAu = author[1]
                diedAu = author[2]
                line = { "Name": nameAu, "Born": bornAu, "Died": diedAu }
                composers.append(line)

            c.execute('SELECT id, name FROM edition WHERE score = (?)', [scoId[0]])
            editionInfo = c.fetchall()

            for data in editionInfo:
                ediId = editionInfo[0][0]
                edition = editionInfo[0][1]

                c.execute('SELECT name, born, died FROM person JOIN edition_author ON edition_author.editor = person.id WHERE edition_author.edition = (?)', [ediId])
                editorInfo = c.fetchall()

                editors = []
                for editor in editorInfo:
                    nameEd = editor[0]
                    bornEd = editor[1]
                    diedEd = editor[2]
                    line = { "Name": nameEd, "Born": bornEd, "Died": diedEd }
                    editors.append(line)
                
                c.execute('SELECT id, partiture FROM print WHERE edition = (?)', [ediId])
                printInfo = c.fetchall()

                printNumber = printInfo[0][0]
                partiture = printInfo[0][1]
                if (partiture is 'N'):
                    partiture = False
                elif (partiture is 'Y' or partiture is 'P' ): 
                    partiture = True
                else: 
                    partiture = None      

                line = { "Print Number": printNumber, "Composer": composers, "Title": title, "Genre": genre, "Key": key, 
                "Composition Year": compositionYear, "Edition": edition, "Editor": editors, "Voices": voices, "Partiture": partiture,
                "Incipit": incipit }
                allComposers.append(line)


        output.update({searchingName: allComposers})
sys.stdout.write(json.dumps(output, indent=4, ensure_ascii=False))