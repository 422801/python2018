#input print number (argument)
#output list of composers (stdout) / each composer is dictionary (name, born, died)

# getprint.py 645 ... [{"name": "text of name", "born": cislo, "died": cislo}]

# select ... from person join score_authors on person.id = score_author.composer ... where print.id = ?

import sqlite3
import sys
import json

printNumber = sys.argv[1]

datFile = "scorelib.dat"
conn = sqlite3.connect(datFile)
c = conn.cursor()

output = []

c.execute('SELECT score FROM print JOIN edition ON print.edition = edition.id WHERE print.id=(?)', [printNumber])
ids = c.fetchall()
peopleId = []
for id in ids:
    c.execute('SELECT person.name, person.born, person.died FROM person JOIN score_author ON score_author.composer = person.id WHERE score_author.score=(?)', [id[0]])
    peopleId = c.fetchall()

for row in peopleId:
    name = row[0]
    born = row[1]
    died = row[2]

    line = { "Name": name, "Born": born, "Died": died }
    output.append(line)


sys.stdout.write(json.dumps(output, indent=4, ensure_ascii=False))