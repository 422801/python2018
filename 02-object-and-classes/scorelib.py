import re

class Voice:
    def __init__(self, name, range):
        self.name = name
        self.range = range

class Person:
    def __init__(self, name, born, died):
        self.name = name
        self.born = born
        self.died = died


class Composition:
    voices = []
    authors = [] #Composer
    
    def __init__(self, name, incipit, key, genre, year):
        self.name = name
        self.incipit = incipit
        self.key = key
        self.genre = genre
        self.year = year

class Edition:
    composition = Composition
    authors = [] #Editor

    def __init__(self, name):
        self.name = name


#Print persons
def printPersons(listOfPersons):
    toPrint = ''
    for person in listOfPersons:
        if (person.name is not None and person.born is None and person.died is None):
            if (len(toPrint) == 0):
                toPrint = person.name
            else: 
                toPrint = toPrint + ", " + person.name 

        elif(person.name is not None and person.born is not None and person.died is None):
            if (len(toPrint) == 0):
                toPrint = person.name + " " + person.born + "--"
            else:
                toPrint = toPrint + ", " + person.name + " " + person.born + "--"
        
        elif(person.name is not None and person.born is None and person.died is not None):
            if (len(toPrint) == 0):
                toPrint = person.name + " " + "--" + person.died
            else:
                toPrint = toPrint + ", " + person.name + " " + "--" + person.died
        
        elif(person.name is not None and person.born is not None and person.died is not None):
            if (len(toPrint) == 0):
                toPrint = person.name + " " + person.born + "--" + person.died
            else:
                toPrint = toPrint + ", " + person.name + " " + person.born + "--" + person.died
    return toPrint

#Print line of file
def printOutput(nameOfLine, givenString):
    if (givenString is None or givenString == "None"):
        print(nameOfLine)
    else:
        print(nameOfLine + givenString)   


class Print:
    edition = Edition
    
    def __init__(self, print_id, partiture):
        self.print_id = print_id
        self.partiture = partiture
    
    #reconstructs and prints the original stanza
    def format(self):
        #Print Number
        print("Print Number: " + str(self.print_id))
        
        #Composer
        composersToPrint = printPersons(self.composition().authors)
        print("Composer: " + composersToPrint)
        
        #Title
        printOutput("Title: ", self.composition().name)
        
        #Genre
        printOutput("Genre: ", self.composition().genre)

        #Key
        printOutput("Key: ", self.composition().key)

        #Composition Year
        printOutput("Composition Year: ", str(self.composition().year))

        #Edition
        printOutput("Edition: ", str(self.edition.name))

        #Editor
        editorsToPrint = printPersons(self.edition.authors)
        print("Editor: " + editorsToPrint)

        #Voice
        i = 1
        for voice in self.composition().voices:
            string = "Voice " + str(i) + ": "
            if (voice.name is None and voice.range is None):
                print(string)
            elif (voice.name is None and voice.range is not None):
                print(string + voice.range)
            elif (voice.name is not None and voice.range is None):
                print(string + voice.name)
            elif (voice.name is not None and voice.range is not None):
                print(string + voice.range + ", " + voice.name)
            i += 1
        

        #Partiture
        if (self.partiture is True):
            print("Partiture: yes")
        else:
            print("Partiture: no")
    
        #Incipit
        printOutput("Incipit: ", self.composition().incipit)
    
    #Return composition of edition from print
    def composition(self):
        return self.edition.composition

#Check length of line
def checkLength(line):
    if(len(line) is 0 or line is " "):
        return None
    else:
        return line.strip()

#Check line with person and create new person
def yearParser(line):
    person = Person
    born = None
    died = None
    name = checkLength(line)
    
    ryear = re.compile(r'(.*) (\((.*)\))')
    myear = ryear.match(line)
    if (myear is not None):
        name = myear.group(1)
        years = myear.group(2)

        #(+year)
        r = re.compile(r'\(\+(\d{4})\)')
        m = r.match(years)
        if (m is not None):
            died = m.group(1)

        #(*year)
        r = re.compile(r'\(\*(\d{4})\)')
        m = r.match(years)
        if (m is not None):
            born = m.group(1)

        #(year--)     
        r = re.compile(r'\((\d{4})-')
        m = r.match(years)
        if (m is not None):
            born = m.group(1)
                        
        #(--year)     
        r = re.compile(r'-(\d{4})\)')
        m = r.search(years)
        if (m is not None):        
            died = m.group(1) 
    
    person = Person(name, born, died)
    return person
        
#Line with the composers
def composers(line):
    persons = []
    composers = []

    r = re.compile('(.*);')
    m = r.match(line)
    if (m is None):
        persons.append(yearParser(line))
    
    else:
        composers = line.split('; ')
        for composer in composers:
            persons.append(yearParser(composer))

    return persons

#Composition year
def compositionYear(line):
    r = re.compile(r'(\d{4})')
    m = r.match(line)
    if (m is not None):
        return int(m.group())
    else:
        return None

#Check partiture
def partitureString(line):
    r = re.compile('yes')
    m = r.match(line)
    if (m is not None):
        return True
    else:
        return False

#Editors line
def editorLines(line):
    editors = []
    names = []
    line = line.strip()
    r = re.compile('(.*), ')
    m = r.match(line)
    if (m is None): # doesn't contain ", " -> only one name
        editors.append(yearParser(line))
    else:
        names = line.split(', ')
        i = 0
        for name in names:
            notNameOnly = []
            rr = re.compile('(.*) (.*)') #name surname
            mm = rr.match(name)
            if (mm is not None): 
                notNameOnly = name.split(" ")
                onlyName = ''
                for text in notNameOnly:
                    if ((text[0].islower()) is not True): #start with lower case word is not a name
                        onlyName = onlyName + " " + text
                        onlyName = onlyName.lstrip() 
            
                editors.append(yearParser(onlyName))

            else: #surname, name
                if (i <= (len(names)/2)):
                    editors.append(yearParser(names[i+1] + " " + names[i]))
                    i += 2

    return editors

#Voice lines
def voiceLines(line):
    name = None
    range = None
    voice = Voice 

    words = []
    words = line.split(" ")
    foundRange = False

    name = checkLength(line)

    for word in words:
        r = re.compile(r'(.*)--(.*)')
        m = r.search(word)
        newRange = None
        if (m is not None and foundRange is False):
            foundRange = True
            newRange = m.group()
            range = newRange.replace(",", "")
            range = newRange.replace(";", "")
               
            name = line.replace(newRange, '').strip()
    
    voice = Voice(name, range)
    return voice

#Load file and return list of Print instances
def load(fileName):
    data = []

    instanceOfPrint = Print
    print_id = int #Print Number
    partiture = bool #Partiture
    
    edition = Edition 
    editionName = str
    
    composition = Composition
    compositionName = str #Title
    incipit = str #Incipit
    key = str #Key
    genre = str #Genre
    year = int #Composition Year

    voices = [] # list of Voices -> Voice N    voices.append(Voice(name, range))
    compositionAuthors = [] # list of Authors -> Composer   authors.append(Person(name, born, died))
    editorAuthors = [] # list of Authors -> Editor   authors.append(Person(name, born, died))          "priezvisko, meno" -> "slovo, slovo" => slovo, slovo slovo (2 osoby)


    with open(fileName, encoding='utf-8') as file:
      
        for line in file:
            p = re.compile("\n")
            emptyLine = p.match(line)

            if (emptyLine is None):
                pe = re.compile("(.*):(.*)")
                text = pe.match(line)
                
                if (text is not None):
                    #print(text.group(1)) #Name of data
                    #print(text.group(2)) #Data
                    vo = re.compile("Voice (.*)")
                    voiceMatch = vo.match(text.group(1))
                    information = text.group(2).lstrip()

                    if (text.group(1) == "Print Number"):
                        print_id = int(information)

                    elif (text.group(1) == "Composer"):
                        compositionAuthors = composers(information)
                        
                    elif (text.group(1) == "Title"):
                        compositionName = checkLength(information)

                    elif (text.group(1) == "Genre"):
                        genre = checkLength(information)

                    elif (text.group(1) == "Key"):
                        key = checkLength(information) 

                    elif (text.group(1) == "Composition Year"):
                        year = compositionYear(information)

                    elif (text.group(1) == "Edition"):
                        editionName = checkLength(information) 

                    elif (text.group(1) == "Editor"):
                        editorAuthors = editorLines(information)
                        
                    elif (text.group(1) == "Partiture"):
                        partiture = partitureString(information)                

                    elif (voiceMatch is not None):
                        voices.append(voiceLines(information))

                    elif (text.group(1) == "Incipit"):
                        incipit = checkLength(information) 

                        composition = Composition(compositionName, incipit, key, genre, year)
                        composition.voices = voices
                        voices = []
                        composition.authors = compositionAuthors
                        compositionAuthors = []

                        edition = Edition(editionName)
                        edition.authors = editorAuthors
                        editorAuthors = []
                        edition.composition = composition

                        instanceOfPrint = Print(print_id, partiture)
                        instanceOfPrint.edition = edition

                        data.append(instanceOfPrint)   
        
    return data