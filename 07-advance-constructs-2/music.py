"""
posuvame windows o 0.1 
ratam peaky pre 0.1 ale pozeram okna po jednej sekunde

pouzivame logaritmus na prepocet medzi jednotlivymi notami(pismenami), notovy system, oktavy a = 440Hz , Bez = 440 * 12odmocnina z 2 ..., 
rozdiel je 100 centov podla logaritmu cize si vies prevadzat tie Hz na zaklade logaritmu

v logaritmickej skale su rovnako od seba

440 a  do 880  a  je jedna oktava
"""


import sys
import numpy as np
import wave, struct
from math import log2
# invocation: ./music.py 440 audio.wav
fileInput = sys.argv[2] #audio.wav
inputFreq = int(sys.argv[1]) # is the frequency of the pitch a’

# print pitch of frequency
def pitch(freq, inputFreq):
    PITCHES = ("c", "cis", "d", "es", "e", "f", "fis", "g", "gis", "a", "bes", "b")
    
    octave = 12
    scaled = inputFreq * pow(2, - (octave + 9)/octave)
    distance = octave * (log2(freq) - log2(scaled))

    cents = int(100 * (distance % 1))
    indexOfTone = int(distance % octave)
    indexOfOctave = int(distance // octave)
    
    if (cents > 50):
        indexOfTone += 1
        cents -= 100

    if (indexOfTone >= octave):
        indexOfTone -= octave
        indexOfOctave += 1

    tone = PITCHES[indexOfTone]

    if (indexOfOctave < 0):
        suffix = "," * ((-1 * indexOfOctave) - 1)
        return(tone.upper() + suffix + ("+" if cents >= 0 else "") + str(cents))
    else:
        suffix = "'" * indexOfOctave
        return(tone + suffix + ("+" if cents >= 0 else "") + str(cents))

def sampleDatas(fileInput):
    with wave.open(fileInput, 'r') as waveFile:

        length = waveFile.getnframes()
        rate = waveFile.getframerate()  # frequecy
        channels = waveFile.getnchannels()

        #windows = int(length / rate)

        frames = waveFile.readframes(length)

    sampleData = struct.unpack("{}h".format(length * channels), frames)
                
    if (channels is 2):
        sampleData = [sum(x)/2 for x in zip(sampleData[0::2], sampleData[1::2])]

    slidingWindow = int(rate * 0.1)

    data = [sampleData[index:index+rate]
            for index in range(0, len(sampleData) - rate, slidingWindow)] # range([start], stop[, step])
    return data

def maxPeaks(data):
    maxPeaks = [] 
    for da in data:
        ampArray = np.abs(np.fft.rfft(da))
        average = sum(ampArray) / len(ampArray)
        peaks = []
        maxPeaksInWindow = []
        for n in enumerate(ampArray, start=0): 
            if (n[1] >= (20 * average)):
                peaks.append(n)

        count = len(peaks)
        if (count > 3):
            peaks = sorted(peaks, key=lambda tup:(-tup[1], tup[0]))
            for i in range(3):
                maxId = peaks[i][0]
                maxPeaksInWindow.append(maxId)
        elif (count > 0 and count <=3):
            for peak in peaks:
                maxId = peak[0]
                maxPeaksInWindow.append(maxId)
        
        if (len(maxPeaksInWindow) is not 0):
            maxPeaksInWindow.sort()
            maxPeaks.append(maxPeaksInWindow) 
    return maxPeaks

# clusters = {'peaks': peak, 'count': count} -> [{'peaks': [62, 220, 440], 'count': 20}, ..]
# clusters
def clustersData(maxPeaks):
    if (len(maxPeaks) is not 0):
        clusters = []
        count = 1
        for peak in maxPeaks:     
            
            if (len(clusters) is not 0):
                if (len(clusters[-1]['peaks']) == len(peak)):
                    isTheSame = 0
                    for i in range(len(peak)):
                        if (clusters[-1]['peaks'][i] == peak[i]): # - 1 Hz
                            isTheSame += 1

                    if (len(peak) == isTheSame):
                        clusters[-1]['count'] += 1
                    else:
                        cluster = {'peaks': peak, 'count': count}
                        clusters.append(cluster)
                else:
                    cluster = {'peaks': peak, 'count': count}
                    clusters.append(cluster)
            else:
                cluster = {'peaks': peak, 'count': count}
                clusters.append(cluster)
    return clusters

def main():
    sampleData = sampleDatas(fileInput)
    maxPeaksData = maxPeaks(sampleData)
    clusters = clustersData(maxPeaksData)

    start = 0.0
    end = 0.1
    for cluster in clusters:
        outputLine = []
        
        for peak in cluster['peaks']:
            outputLine.append(pitch(peak, inputFreq))
        
        output = ' '.join(outputLine)
        end = start + cluster['count'] * 0.1
                
        print('%.1f-%.1f %s' % (start, end, output))

        start = end

if __name__ == '__main__':
    main()