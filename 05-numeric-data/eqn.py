# print the solution to stdout (using variable names)
# variables → single letters, coefficients → integers

# if unique solution exist print solution. f.e.: 2x + 3y = 5; x - y = 0; solution: x = 1, y = 1
# print no solution if the system is inconsistent. f.e.: x + y = 4; x + y = 5; solution: no solution
# Multiple Solutions, only print the dimension of the solution space. f.e.: x + y - z = 0; x = 0; solution: solution space dimension: 1

# constants on the right side only, solution to stdout in alphabetical order, spaces around operators and =; no space between a coefficient and a variable

import sys
import re
import numpy

inputText = sys.argv[1] #input.txt
coefficients = []
constants = []

with open(inputText, "r", encoding="utf-8") as f:
    regex = re.compile('[a-z]')
    regexConstant = re.compile('(.*) = (.*)')
    uniqueVariables = set()

    for lin in f:
        variables = regex.findall(lin)
        for variable in variables:
            uniqueVariables.add(variable)
        allVariables = sorted(uniqueVariables)

    f.seek(0)
    for line in f:                  
        constant = regexConstant.search(line)
        constants.append(constant.group(2))

        dataLine = constant.group(1)
        dataLine = dataLine.replace(' ', '')
        dataLine = dataLine.replace('-', ' -')
        dataLine = dataLine.replace('+', ' ')
        striped = dataLine.split(' ')

        coefOfLine = []
        for varia in allVariables:
            wasFound = False
            for data in striped:
                rege = re.compile(varia)
                found = rege.search(data)
                if (found is not None):
                    wasFound = True
                    data = data.replace(varia, '')
                    if (len(data) == 0):
                        data = '1'
                    if (len(data) == 1 and data == '-'):
                        data = data.replace('-', '-1')

                    coefOfLine.append(data)
                    
            if (wasFound is False):
                data = '0'
                coefOfLine.append(data)
        coefficients.append(coefOfLine)

coefficients = numpy.asarray(coefficients, dtype=numpy.float64)
constants = numpy.asarray(constants, dtype=numpy.float64)

n = len(allVariables)
coefMatRank = numpy.linalg.matrix_rank(coefficients)
augmentedMat = []

bla = coefficients.tolist()

i = 0
for data in bla:
    line = data
    line.append(constants[i])
    augmentedMat.append(data)
    i += 1   

argMatRank = numpy.linalg.matrix_rank(augmentedMat)

if (coefMatRank == argMatRank):
    if (coefMatRank == n):
        result = numpy.linalg.solve(coefficients, constants)
        i = 0
        output = 'solution: '
        for atr in result:
            output = output + str(allVariables[i]) + ' = ' + str(atr)
            i += 1
            if (i < n):
                output = output + ', '
    
        sys.stdout.write(str(output))

    else: 
        sys.stdout.write("solution space dimension: " + str(n - coefMatRank))

else: # is inconsistent if the rank of the augmented matrix is greater than the rank of the coefficient matrix
    sys.stdout.write("no solution")