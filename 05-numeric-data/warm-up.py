import sys
import numpy.linalg
a = [[1, 2, 7], [3, 4, 8], [5, 6, 9]]

sys.stdout.write(str(numpy.linalg.det(a)))
sys.stdout.write("\n")
sys.stdout.write(str(numpy.linalg.matrix_rank(a)))
sys.stdout.write("\n")
sys.stdout.write(str(numpy.linalg.inv(a)))