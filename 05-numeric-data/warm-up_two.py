import sys
import numpy.linalg
import numpy

coefficients = [[2, 3], [1, -1]]
constants = [5, 0]
result = numpy.linalg.solve(coefficients, constants)

print(coefficients)
print(constants)

sys.stdout.write(str(result))