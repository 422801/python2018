import re
import sys
import math
from collections import Counter
from collections import OrderedDict

file = sys.argv[1]

# Short name values
def shortName(value):
    newValue = ''
    value = value.replace('-', ' ')
    lastCharacter = value[-1:]
    if (lastCharacter is ','):
        return value[:-1]
    names = value.split(' ')
    if (len(names) > 1):        
        newValue = newValue + names[0]

        if (newValue[-1:] is not ','):
            newValue = newValue + ', '
        else:
            newValue = newValue + ' '

        for name in names[1:]:
            name = name[0].upper() + '. '
            newValue = newValue + name 
        newValue = newValue[:-1]
        return newValue
    else:
        return value

# Multiple Composers in line
def multipleComposers(value, ctr, data):
    composers = []
    r = re.compile('(.*);')
    m = r.match(value)
    if (m is None):
        return False

    composers = value.split('; ')
    for composer in composers:
        composer = deleteBrackets(composer)
        #composer = shortName(composer)
        ctr[composer] += 1
        data[composer] = ctr[composer]    
    return True               
     

# Delete brackets from line
def deleteBrackets(value):
    newValue = value
    r = re.compile(r'(.*) \([+\d{4}/-]*\)')
    m = r.match(value)
    if (m is not None):
        newValue = m.group(1)
    return newValue 

# list the dictionary with unique Composer and count of appearance 
def composer():
    data = {}
    ctr = Counter()
    f = open(file, encoding="utf-8")
    for line in f:
        p = re.compile("Composer: (.*)")
        m = p.match(line)
        if (m is not None and m.group(1) is not ''):
            value = deleteBrackets(m.group(1))
            if (multipleComposers(value, ctr, data) is not True):
                value = value.rstrip()
                ctr[value] += 1
                data[value] = ctr[value]

    for k, v in data.items():
        print('%s: %d' % (k, v))  


# Year to century
def centuryFromYear(year):
    result = math.ceil(year/100)
    if (result == 21):
        return str(result) + 'st century'
    return str(result) + 'th century' 

# Count of compositions in century
def century():
    data = {}
    ctr = Counter()
    f = open(file, encoding="utf-8")
    for line in f:
        p = re.compile("Composition Year: (.*)")
        m = p.search(line)
        if (m is not None):
            yearParser = re.compile(r"(\d{4})")
            year = yearParser.search(m.group(1))

            centuryParser = re.compile("(.*) century")
            century = centuryParser.search(m.group(1))

            if (year is not None):
                result = centuryFromYear(int(year.group()))
                ctr[result] += 1
                data[result] = ctr[result]

            if (century is not None):
                result = century.group()
                ctr[result] += 1
                data[result] = ctr[result]

    for k, v in data.items():
        print('%s: %d' % (k, v))


if (sys.argv[2] == "composer"):
    composer()
    
if (sys.argv[2] == "century"):
    century()
