'''

invocation: ./http-forward.py 9001 example.com
− listen on the specified port (9001 above) for HTTP
− use example.com as the upstream for GET

'''
import sys 
import json
import http
from http.server import HTTPServer, BaseHTTPRequestHandler
import urllib.request
import socket
from urllib.error import HTTPError


input_port = sys.argv[1]
input_url = sys.argv[2] # upstream for GET

class requestHandler(BaseHTTPRequestHandler):

    def urlopen_output(self, res_status, res=None, headers=None):
        output = {}
        
        output["code"] = res_status
        if headers:
            output["headers"] = headers 

        if res:
            try:
                output["json"] = json.loads(res)
            except ValueError:
                output["content"] = res       
        return output

    def send_output(self, output):
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        self.wfile.write(bytes(json.dumps(output, indent=4, ensure_ascii = False), 'UTF-8'))


    def do_GET(self):
        if (input_url[:4] != 'http'):

            upstream = 'http://' + input_url
        else:
            upstream = input_url

        output = {}
        headers = dict(self.headers)
        request = urllib.request.Request(url = upstream, headers = headers, method = "GET")

        try:
            with urllib.request.urlopen(request, timeout=1) as r:

                output["code"] = r.status
                output["headers"] = dict(r.getheaders()) 
                content = r.read().decode('utf-8')
                # content = r.read()
        except (socket.timeout, urllib.error.URLError):
            # urllib.error.URLError:
            output['code'] = "timeout" 
            return output

        try:
            output["json"] = json.loads(content)
        except ValueError:
            output["content"] = str(content)

        self.send_output(output)
        

# send response self.send_response(200), self.add_headers

    def do_POST(self):
        output = {}
        try:
            length = int(self.headers['Content-Length'])
            data_content = json.loads(self.rfile.read(length).decode('utf-8'))
    
            if ("type" in data_content):
                request_type = data_content["type"]
            else:
                request_type = "GET"

            if (data_content["type"] == "POST" and ("content" not in data_content or "url" not in data_content)):
                raise ValueError
            
            url = data_content["url"]
            timeout = int(data_content["timeout"]) if "timeout" in data_content else 1
            headers = data_content["headers"] if "headers" in data_content else dict()
            content = data_content["content"] if request_type == "POST" else None

            #data = bytes(content if "content" in data_content else '', 'UTF-8')
            if content:
                content = json.dumps(content).encode()

            request = urllib.request.Request(url=url, data=content, headers=headers, method=request_type)
            try:
                with urllib.request.urlopen(request, timeout=timeout) as r:
    
                    res = r.read().decode('UTF-8')
                    output = self.urlopen_output(r.status, res, dict(r.getheaders()))
                    
                    self.send_output(output)
            except HTTPError as e:
                output = self.urlopen_output(e.code, None, None)
                self.send_output(output)
            except:
                output = self.urlopen_output("timeout", None, None)
                return self.send_output(output)
        except ValueError:
            # output["code"] = "invalid json"
            output = self.urlopen_output("invalid json", None, None)
            self.send_output(output)

def main():

    httpd = HTTPServer(("localhost", int(input_port)), requestHandler)
    httpd.serve_forever()

if __name__ == "__main__":
    main()