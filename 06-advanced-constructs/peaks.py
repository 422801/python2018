"""
    - vstup bude používat signed reprezentaci
    
    - the output goes to stdout

wav file - sample, channel, sample rate (number of samples that exist for each second of data - in Hz)
         - bit depth / bits per sample - number of bits avaible for one sample (v nasom pripade 16-bit)

sample rate: 8 - 48 kHz
channel: stereo or mono (2 or 1)
bit sample rate: 16-bit


average the channels for stereo input
ignore the final (incomplete) analysis window
you can use struct.unpack to decode the samples


OUTPUT:
a peak is a frequency component with amplitude ≥ 20𝑎
− where 𝑎 is the average amplitude in the same window
print the highest- and lowest-frequency peak encountered

in the form low = 37, high = 18000
− print no peaks if there are no peaks
− the numbers are in Hz, precision = exactly 1Hz

think about how precision relates to N


"""

import sys
import numpy as np
import wave, struct


fileInput = sys.argv[1] #audio.wav

with wave.open(fileInput, 'r') as waveFile:

    length = waveFile.getnframes()
    rate = waveFile.getframerate()  # frequecy
    channels = waveFile.getnchannels()

    highestPeak = None
    lowestPeak = None

    fromData = 0
    toData = rate

    time = int(length / rate)

    for i in range (0, time): # loop thru windows
        a = [] # data of one window

        for j in range (fromData, toData): # loop thru window's data       
            waveData = waveFile.readframes(1)       

            if (channels is 1):
                window = struct.unpack("h", waveData)       
                a.append(int(window[0]))
            
            elif (channels is 2):
                data = struct.iter_unpack("h", waveData)
                values = []
                for da in data:
                    values.append(int(da[0]))
                a.append(sum(values) / 2)        
    
        fromData = toData
        toData = toData + rate
        if (toData > length):
            toData = length

        ampArray = np.abs(np.fft.rfft(np.array(a)))  # np.where(ampArray==number)
        average = sum(ampArray) / len(ampArray)

        highPeak = None
        lowPeak = None

        for n in enumerate(ampArray, start=0): 
            if (n[1] >= (20 * average)):
                if (highPeak is None and lowPeak is None):
                    highPeak = n[0]
                    lowPeak = n[0]
                if (n[0] >= highPeak):
                    highPeak = n[0]
                if (n[0] <= lowPeak):
                    lowPeak = n[0]

        if (highPeak is not None):
            if (highestPeak is None):
                highestPeak = int(highPeak)
            elif (highestPeak < highPeak):
                highestPeak = int(highPeak)

        if (lowPeak is not None):
            if (lowestPeak is None):
                lowestPeak = int(lowPeak)
            elif (lowestPeak > lowPeak):
                lowestPeak = int(lowPeak)

    if (highestPeak is None and lowestPeak is None):
        sys.stdout.write("no peaks")
    else:
        sys.stdout.write("low = " + str(lowestPeak) + ", high = " + str(highestPeak))