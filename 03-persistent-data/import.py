import sqlite3
import sys
import scorelib
import array

file = sys.argv[1]
datFile = sys.argv[2]

def listOfPersons(authorName, persons, author): 
    for person in persons:
        if (person.name == authorName):
            if (author.born is not None):
                person.born = author.born
            if (author.died is not None):    
                person.died = author.died

conn = sqlite3.connect(datFile)
c = conn.cursor()

c.execute('CREATE TABLE person ( id integer primary key not null, born integer, died integer, name varchar not null )')
c.execute('CREATE TABLE score ( id integer primary key not null, name varchar, genre varchar, key varchar, incipit varchar, year integer )')
c.execute('CREATE TABLE voice ( id integer primary key not null, number integer not null, score integer references score( id ) not null, range varchar, name varchar )')
c.execute('CREATE TABLE edition ( id integer primary key not null, score integer references score( id ) not null, name varchar, year integer )')
c.execute('CREATE TABLE score_author ( id integer primary key not null, score integer references score( id ) not null, composer integer references person( id ) not null )')
c.execute('CREATE TABLE edition_author ( id integer primary key not null, edition integer references edition( id ) not null, editor integer references person( id ) not null )')
c.execute('CREATE TABLE print ( id integer primary key not null, partiture char(1) default "N" not null, edition integer references edition( id ) )')
conn.commit()


listOfPrint = scorelib.load(file)
persons = []
compositions = []
editions = []
seen = set()
prints = []
voices = []



#Loop thru Prints
for singlePrint in listOfPrint:

#Unique Print
    duplicatePri = False
    if (len(prints) == 0):
        prints.append(singlePrint)
    for prin in prints:
        duplicatePri = scorelib.Print.isEqual(prin, singlePrint)
    if (duplicatePri is False):
        prints.append(singlePrint)

#PERSON
    #authors from composition
    for author in singlePrint.edition.composition.authors:
        authorName = author.name
        if (authorName not in seen and authorName is not None):
            persons.append(author)
            seen.add(authorName)
        listOfPersons(authorName, persons, author)
    
    #editors from edition
    for editor in singlePrint.edition.authors:
        editorName = editor.name
        if (editorName not in seen and editorName is not None):
            persons.append(editor)
            seen.add(editorName)
        listOfPersons(editorName, persons, editor)

#SCORE 
    composition = singlePrint.edition.composition
    duplicite = False
    
    if len(compositions) is 0:
        compositions.append(composition)
    else:
        for compi in compositions:
            duplicite = scorelib.Composition.isEqual(compi, composition)
        if (duplicite is False):
            compositions.append(composition)
        
#EDITION 
    dupliciteEdi = False
    edition = singlePrint.edition
    if len(editions) is 0:
        editions.append(edition)
    else:
        for edi in editions:
            dupliciteEdi = scorelib.Edition.isEqual(edi, edition)
        if (dupliciteEdi is False):
            editions.append(edition)

#Filling Tables

#unique people
for person in persons:
    c.execute('INSERT INTO person (born, died, name) VALUES (?, ?, ?)', (person.born, person.died, person.name))   
conn.commit()

#score, voice, score_author
for compos in compositions:
    #unique score
    c.execute('INSERT INTO score (name, genre, key, incipit, year) VALUES (?, ?, ?, ?, ?)', (compos.name, compos.genre, compos.key, compos.incipit, compos.year))
    scoreId = c.lastrowid    

    #voices from unique score
    compositionVoices = compos.voices
    i = 1 #which Voice this is
    for voice in compositionVoices:
        c.execute('INSERT INTO voice (number, score, range, name) VALUES (?, ?, ?, ?)', (i, scoreId, voice.range, voice.name))
        i += 1

    #score_author
    for author in compos.authors:
        c.execute('SELECT id FROM person WHERE name = (?)', [author.name])
        
        autID = c.fetchone()
        if (autID is not None):
            c.execute('INSERT INTO score_author (score, composer) VALUES (?, ?)', (scoreId, autID[0]))

    #edition, edition_author
    for edi in editions:
    #unique edition
        com = edi.composition
        if (scorelib.Composition.isEqual(compos, com)):
            c.execute('INSERT INTO edition (score, name, year) VALUES (?, ?, ?)', (scoreId, edi.name, None))
            editionIndex = c.lastrowid

            #edition_author
            for edAuhtor in edi.authors:
                c.execute('SELECT id FROM person WHERE name = (?)', [edAuhtor.name])
                edID = c.fetchone()
                if (edID is not None):
                    c.execute('INSERT INTO edition_author (edition, editor) VALUES (?, ?)', (editionIndex, edID[0]))         
conn.commit()


#print
for pri in prints:
    #unique print
    edi = pri.edition
    combID = 0
    i = 0
    com = edi.composition

    for editi in editions:
        if (scorelib.Edition.isEqual(edi, editi)):
            com = edi.composition
    
    for comb in compositions:
        i += 1
        if (scorelib.Composition.isEqual(com, comb)):
            combID = i

    partitur = "N"
    if (pri.partiture):
        partitur = "Y"
    elif (pri.partiture is False): 
        partitur = "N"
    else:
        partitur = "P"

    #print(combID, edi.name)
    c.execute('SELECT id FROM edition WHERE score=(?)', [combID])
    ediID = c.fetchone()
    if (ediID is not None):
        c.execute('INSERT INTO print (id, partiture, edition) VALUES (?, ?, ?)', (pri.print_id, partitur, ediID[0]))

conn.commit()


def testPrint():
    c.execute('SELECT * FROM print')
    rows = c.fetchall()
    for row in rows:
        print(row) 
    
testPrint()

